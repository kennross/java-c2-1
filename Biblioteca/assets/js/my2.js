/*
1. Declarar variables
2. Asignar datos a las variables
3. Validación de variables
4. Retroalimentación
*/

var dni = null, 
nombre = null,
apellido = null,
tipo = null,
departamento = null,
ciudad = null;


// Cualquier cosa que no se entienda me la preguntan
function validarCrearAutor() {
	dni = $('#dni').val();
	nombre = $('#nombre').val();
	apellido = $('#apellido').val();
	tipo = $('#tipo').val();
	departamento = $('#departamento').val();
	ciudad = $('#ciudad').val();

	/*
	1. Ir al contenedor (form-group) del input
	2. Alternar clases (has-error y has-success)
	3. Agrego mensaje de retroalimentación
	*/
	$('#dni').parent('.form-group').children('span').text('');
	$('#dni').parent('.form-group').removeClass('has-error has-success');
	if (dni == '') {
		/* Mensajes de retroalimentación (No puede ser nulo) */
		$('#dni').parent('.form-group').addClass('has-error');
		$('#dni').parent('.form-group').children('span').text('No puede ser un campo vacío');
		return false;
		// console.warn('Entró al NULL');
	} else if (dni.length > 10) {
		/* Mensajes de retroalimentación (No puede ser mayor que 10) */
	} else if (dni.length < 6) {
		/* Mensajes de retroalimentación (No puede ser menor que 6) */
	} else if (isNaN(dni)) {
		/* Mensajes de retroalimentación (No es un número) */
	} else {
		$('#dni').parent('.form-group').removeClass('has-error');
		$('#dni').parent('.form-group').addClass('has-success');
	}	
}