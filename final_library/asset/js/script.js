var myLanguage = {
	errorTitle: '¡Envío del formulario fallido!',
	requiredFields: 'No ha respondido a todos los campos requeridos',
	badTime: 'No ha dado una hora correcta',
	badEmail: 'No ha dado una dirección de correo electrónico correcta',
	badTelephone: 'No ha dado un número de teléfono correcto',
	badSecurityAnswer: 'No ha dado una respuesta correcta a la pregunta de seguridad',
	badDate: 'No ha dado una fecha correcta',
	lengthBadStart: 'El valor de entrada debe estar entre ',
	lengthBadEnd: ' caracteres',
	lengthTooLongStart: 'El valor de entrada es mayor que ',
	lengthTooShortStart: 'El valor de entrada es más corto que ',
	notConfirmed: 'Los valores de entrada no pudieron ser confirmados',
	badDomain: 'Ámbito de los valores incorrectos',
	badUrl: 'El valor de entrada no es una URL correcta',
	badCustomVal: 'El valor de entrada es incorrecta',
	andSpaces: ' y espacios ',
	badInt: 'El valor de entrada no es un número correcto',
	badSecurityNumber: 'Su número de la seguridad social era incorrecta',
	badUKVatAnswer: 'Número incorrecto IVA del Reino Unido',
	badStrength: 'La contraseña no es lo suficientemente fuerte',
	badNumberOfSelectedOptionsStart: 'Tienes que elegir al menos ',
	badNumberOfSelectedOptionsEnd: ' respuestas',
	badAlphaNumeric: 'El valor de entrada sólo puede contener caracteres alfanuméricos ',
	badAlphaNumericExtra: ' y ',
	wrongFileSize: 'El archivo que está tratando de subir es demasiado grande (máximo %s)',
	wrongFileType: 'Sólo los archivos de tipo %s están permitidos',
	groupCheckedRangeStart: 'Por favor, elija entre ',
	groupCheckedTooFewStart: 'Por favor, elija al menos ',
	groupCheckedTooManyStart: 'Por favor, elija un máximo de ',
	groupCheckedEnd: ' item(s)',
	badCreditCard: 'El número de tarjeta de crédito no es correcta',
	badCVV: 'El número CVV no es correcto',
	wrongFileDim : 'Dimensiones de imagen incorrectos,',
	imageTooTall : 'la imagen no puede ser más alto que',
	imageTooWide : 'la imagen no puede ser más amplio que',
	imageTooSmall : 'la imagen es demasiado pequeño',
	min : 'mínimo',
	max : 'máximo',
	imageRatioNotAccepted : 'El radio de la imagen no es aceptada',
	requiredField: 'Este campo es obligatorio'
};

$.validate({
	language : myLanguage
});

$('input[name="estado"]').change(function() {
	if (this.value == 'deteriorado') {
		$('#boxComment').show(200);
	} else {
		$('#boxComment').hide(300);
	}
});

$('#tipo').change(function() {
	$('#boxNumeroCanciones, #boxDuracion, #boxNumeroPaginas').hide();
	switch (this.value) {
		case 'cd': 
		$('#boxNumeroCanciones').show(200);
		break;

		case 'pelicula': 
		$('#boxDuracion').show(200);
		break;

		case 'libro': 
		$('#boxNumeroPaginas').show(200);
		break;
	}
});